Rust library for reading values broadcasted by Xiaomi bluetooth Thermometer LYWSD03MMC. Works with alternate
firmware from https://github.com/atc1441/ATC_MiThermometer

## Usage

```rust
fn main() -> Result<(), dbus::Error> {
    let sensor = atc_lywsd03mmc::ATCLYWSD03MMC::new("hci0");
    
    for reading in sensor {
        println!(
            "{} T: {}°C H: {}% B: {}% ({}mV)",
            reading.name,
            reading.temperature,
            reading.humidity,
            reading.battery_percentage,
            reading.battery_mv,            
        );
    }
    Ok(())
}
```
