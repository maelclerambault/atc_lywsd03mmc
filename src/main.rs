use atc_lywsd03mmc::ATCLYWSD03MMC;

pub fn main() -> Result<(), bluer::Error> {
    let sensor = ATCLYWSD03MMC::new();
    
    for reading in sensor {
        println!(
            "{} T: {}°C H: {}% B: {}% ({}mV)",
            reading.name,
            reading.temperature,
            reading.humidity,
            reading.battery_percentage,
            reading.battery_mv,            
        );
    }
    Ok(())
}
