//! Library for reading values broadcasted by Xiaomi bluetooth Thermometer LYWSD03MMC. Works with alternate
//! firmware from https://github.com/atc1441/ATC_MiThermometer
//!
//! # Usage
//!
//! ```
//! let sensor = atc_lywsd03mmc::ATCLYWSD03MMC::new("hci0");
//! 
//! for reading in sensor {
//!   dbg!(&reading);
//! }
//! ```

use bluer::{AdapterEvent, DeviceEvent, DeviceProperty};
use tokio::sync::mpsc::{channel, Sender, Receiver};

const ATC_MI_THERMOMETER_UUID: &str = "0000181a-0000-1000-8000-00805f9b34fb";
const ATC_MI_THERMOMETER_MAC_PREFIX: &[u8] = &[0xA4, 0xC1, 0x38];

/// Contains a single measurement from a device
#[derive(Debug)]
pub struct Reading {
    /// Name of the originator of the measurement
    pub name: String,
    
    /// Temperature in °C
    pub temperature: f32,

    /// Humidity in %
    pub humidity: u8,

    /// Battery level in %
    pub battery_percentage: u8,
    
    /// Battery level in mV
    pub battery_mv: u16,
    
    /// Measurement id
    pub count: u8,
}

/// Gives access to temperature reading over bluetooth
pub struct ATCLYWSD03MMC {
    _thread_id: std::thread::JoinHandle<()>,
    receiver: Receiver<Reading>,
}

impl ATCLYWSD03MMC {
    /// Creates a ATCLYWSD03MMC
    /// Launches a background thread listening to bluetooth events
    // TODO: return a Result
    pub fn new() -> ATCLYWSD03MMC
    {
        let (sender, receiver) = channel(1);
        
        ATCLYWSD03MMC {
            _thread_id: std::thread::spawn(move || {
                worker_loop(sender).unwrap();
            }),
            receiver
        }
    }
}

impl Iterator for ATCLYWSD03MMC {
    type Item = Reading;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.receiver.blocking_recv().unwrap())
    }
}

fn decode_atc_data(name: &str, data: &[u8]) -> Reading {
    let temperature = i16::from_be_bytes([data[6], data[7]]);
    let humidity = data[8];
    let battery_percentage = data[9];
    let battery_mv = u16::from_be_bytes([data[10], data[11]]);
    let count = data[12];
        
    Reading {
        name: name.to_string(),
        temperature: temperature as f32 / 10.0,
        humidity,
        battery_percentage,
        battery_mv,
        count
    }
}

#[tokio::main]
async fn worker_loop(sender: Sender<Reading>) -> bluer::Result<()> {
    use futures::StreamExt;

    let session = bluer::Session::new().await?;
    let adapter = session.default_adapter().await?;
    adapter.set_powered(true).await?;
    let mut discover = adapter.discover_devices().await?;

    while let Some(evt) = discover.next().await {
        match evt {
            AdapterEvent::DeviceAdded(addr) => {
                if ATC_MI_THERMOMETER_MAC_PREFIX == &addr[..3] {
                    let device = adapter.device(addr)?;
                    let alias = device.alias().await?;

                    let mut events = device.events().await?;
                    let adapter_sender = sender.clone();
                    tokio::task::spawn(async move {
                        while let Some(DeviceEvent::PropertyChanged(event)) = events.next().await {
                            match event {
                                DeviceProperty::ServiceData(data) => {
                                    let uuid = ATC_MI_THERMOMETER_UUID.parse().unwrap();
                                    if let Some(atc1441) = data.get(&uuid) {
                                        adapter_sender.send(
                                            decode_atc_data(&alias, atc1441)
                                        ).await.unwrap();
                                    }
                                },
                                _ => ()
                            }
                        }
                    });
                }

            },
            _ => (),
        }
    }


    Ok(())
}

